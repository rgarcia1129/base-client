import { notification } from 'antd'
import axios from 'config/axios'
import firebase from 'config/firebase'

const firebaseAuth = firebase.auth

export async function login(email, password) {
  return firebaseAuth()
    .signInWithEmailAndPassword(email, password)
    .then(() => true)
    .catch(error => {
      notification.warning({
        message: error.code,
        description: error.message,
      })
    })
}

export async function sendEmailVerification() {
  return firebaseAuth()
    .currentUser.sendEmailVerification()
    .then(() => true)
    .catch(error => {
      notification.warning({
        message: error.code,
        description: error.message,
      })
    })
}

export async function loginWithFacebook() {
  const provider = new firebase.auth.FacebookAuthProvider()

  return firebase
    .auth()
    .signInWithPopup(provider)
    .then(result => {
      // This gives you a Facebook Access Token. You can use it to access the Facebook API.
      const token = result.credential.accessToken
      console.log(token)
      // The signed-in user info.
      const { user } = result
      console.log(user)
      // ...
    })
    .catch(error => {
      // Handle Errors here.
      // const errorCode = error.code;
      const errorMessage = error.message
      console.log(errorMessage)
      // The email of the user's account used.
      const { email, credential } = error
      console.log(email)
      // The firebase.auth.AuthCredential type that was used.
      console.log(credential)
      // ...
    })
}

export async function loginWithTwitter() {
  const provider = new firebase.auth.TwitterAuthProvider()

  return firebase
    .auth()
    .signInWithPopup(provider)
    .then(result => {
      // This gives you a the Twitter OAuth 1.0 Access Token and Secret.
      // You can use these server side with your app's credentials to access the Twitter API.
      const { accessToken, secret } = result.credential
      // The signed-in user info.
      const { user } = result
      // ...

      console.log(accessToken)
      console.log(secret)
      console.log(user)
    })
    .catch(error => {
      // Handle Errors here.
      const errorCode = error.code
      console.log(errorCode)
      // var errorMessage = error.message;
      // The email of the user's account used.
      // var {email, credential} = error;
      // The firebase.auth.AuthCredential type that was used.
    })
}

export async function loginWithGoogle() {
  const provider = new firebase.auth.GoogleAuthProvider()

  return firebase
    .auth()
    .signInWithPopup(provider)
    .then(result => {
      // This gives you a Google Access Token. You can use it to access the Google API.
      const { accessToken } = result.credential
      // The signed-in user info.
      const { user } = result
      // ...

      console.log(accessToken)
      console.log(user)
    })
    .catch(error => {
      // Handle Errors here.
      const errorCode = error.code
      console.log(errorCode)
      // var errorMessage = error.message;
      // The email of the user's account used.
      // var {email, credential} = error;
      // The firebase.auth.AuthCredential type that was used.
    })
}

export async function currentAccount() {
  let userLoaded = false
  function getCurrentUser(auth) {
    return new Promise((resolve, reject) => {
      if (userLoaded) {
        resolve(firebaseAuth.currentUser)
      }
      const unsubscribe = auth.onAuthStateChanged(user => {
        userLoaded = true
        unsubscribe()
        resolve(user)
      }, reject)
    })
  }
  return getCurrentUser(firebaseAuth())
}

export async function logout() {
  return firebaseAuth()
    .signOut()
    .then(() => true)
}

export async function firebaseRegister(email, password) {
  return firebaseAuth()
    .createUserWithEmailAndPassword(email, password)
    .then(response => {
      window.localStorage.setItem('baseApp.idToken', response.user.ra)
      return response.user
    })
    .catch(error => {
      notification.warning({
        message: error.code,
        description: error.message,
      })
    })
}

export async function updateFirebaseName(user, firstName, lastName) {
  return user
    .updateProfile({
      displayName: `${firstName} ${lastName}`,
    })
    .then(() => true)
    .catch(error => {
      notification.warning({
        message: error.code,
        description: error.message,
      })
    })
}

export async function signUp(firstName, lastName, email) {
  const authToken = window.localStorage.getItem('baseApp.idToken')
  return axios
    .post(
      'signup',
      {
        firstName,
        lastName,
        email,
      },
      {
        headers: { Authorization: authToken },
      },
    )
    .then(() => true)
    .catch(error => {
      notification.warning({
        message: error.code,
        description: error.message,
      })
    })
}
