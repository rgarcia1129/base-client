import axios from 'axios'

const authHeader = () => {
  const token = window.localStorage.getItem('baseApp.idToken')
  const auth = token ? `${token}` : ''
  return auth
}

axios.defaults.baseURL = process.env.REACT_APP_AXIOS_BASE_URL
axios.defaults.headers.common.Accept = 'application/json'
axios.defaults.headers.common.Authorization = authHeader()

export default axios
