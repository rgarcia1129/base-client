import { all, takeEvery, put, call } from 'redux-saga/effects'
import { push } from 'react-router-redux'
import { notification } from 'antd'
import {
  login,
  currentAccount,
  logout,
  firebaseRegister,
  updateFirebaseName,
  signUp,
  loginWithFacebook,
  loginWithTwitter,
  loginWithGoogle,
  sendEmailVerification,
} from 'services/user'
import actions from './actions'

export function* LOGIN({ payload }) {
  const { email, password } = payload
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: true,
    },
  })
  const success = yield call(login, email, password)
  if (success) {
    notification.success({
      message: 'Logged In',
      description: 'You have successfully logged in to Clean UI React Admin Template!',
    })
    yield put({
      type: 'user/LOAD_CURRENT_ACCOUNT',
    })
  }
}

export function* LOAD_CURRENT_ACCOUNT() {
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: true,
    },
  })

  const response = yield call(currentAccount)
  if (response) {
    const { uid: id, displayName, email, emailVerified, photoURL: avatar } = response
    yield put({
      type: 'user/SET_STATE',
      payload: {
        id,
        displayName,
        name: 'Administrator',
        email,
        avatar,
        role: 'admin',
        authorized: emailVerified,
      },
    })
    if (!emailVerified) {
      yield put(push('/user/verify'))
    }
  }

  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: false,
    },
  })
}

export function* SEND_EMAIL_VERIFICATION() {
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: true,
    },
  })

  const response = yield call(sendEmailVerification)
  if (response) {
    notification.success({
      message: 'Email sent successfully',
      description: 'An email was sent for verification',
    })
  }

  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: false,
    },
  })
}

export function* LOGOUT() {
  yield call(logout)
  yield put({
    type: 'user/SET_STATE',
    payload: {
      id: '',
      name: '',
      role: '',
      email: '',
      avatar: '',
      authorized: false,
      loading: false,
      displayName: '',
    },
  })
}

export function* REGISTER({ payload }) {
  const { email, password, firstName, lastName } = payload
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: true,
    },
  })

  const user = yield call(firebaseRegister, email, password)
  if (user) {
    yield call(updateFirebaseName, user, firstName, lastName)
    yield call(signUp, firstName, lastName, email)

    notification.success({
      message: 'Logged In',
      description: 'You have successfully logged in to Clean UI React Admin Template!',
    })
    yield call(sendEmailVerification)
    yield put({
      type: 'user/LOAD_CURRENT_ACCOUNT',
    })
  }
}

export function* REGISTER_WITH_FACEBOOK() {
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: true,
    },
  })
  yield call(loginWithFacebook)
}

export function* REGISTER_WITH_GOOGLE() {
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: true,
    },
  })
  yield call(loginWithGoogle)
}

export function* REGISTER_WITH_TWITTER() {
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: true,
    },
  })
  yield call(loginWithTwitter)
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.LOGIN, LOGIN),
    takeEvery(actions.LOAD_CURRENT_ACCOUNT, LOAD_CURRENT_ACCOUNT),
    takeEvery(actions.LOGOUT, LOGOUT),
    takeEvery(actions.REGISTER, REGISTER),
    takeEvery(actions.REGISTER_WITH_FACEBOOK, REGISTER_WITH_FACEBOOK),
    takeEvery(actions.REGISTER_WITH_GOOGLE, REGISTER_WITH_GOOGLE),
    takeEvery(actions.REGISTER_WITH_TWITTER, REGISTER_WITH_TWITTER),
    takeEvery(actions.SEND_EMAIL_VERIFICATION, SEND_EMAIL_VERIFICATION),
    LOAD_CURRENT_ACCOUNT(), // run once on app load to check user auth
  ])
}
