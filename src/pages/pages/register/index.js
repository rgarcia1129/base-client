import React from 'react'
import { Helmet } from 'react-helmet'
import styles from './style.module.scss'
import SignUpForm from './signUpForm'

class Register extends React.Component {
  render() {
    return (
      <div>
        <Helmet title="Register" />
        <section
          className={`${styles.login} ${styles.fullscreen}`}
          style={{ backgroundImage: `url('resources/images/photos/1.jpeg')` }}
        >
          <header className={styles.header}>
            <a className={styles.logo} href="javascript: void(0);">
              <img src="resources/images/logo-inverse.png" alt="Clean UI Admin Template" />
            </a>
            <nav className={styles.loginNav}>
              <ul className={styles.navItems}>
                <li>
                  <a href="javascript: void(0);">&larr; Back</a>
                </li>
                <li>
                  <a className={styles.active} href="javascript: void(0);">
                    Login
                  </a>
                </li>
                <li>
                  <a href="javascript: void(0);">About</a>
                </li>
                <li>
                  <a href="javascript: void(0);">Support</a>
                </li>
              </ul>
            </nav>
          </header>
          <div className={styles.content}>
            <div className={styles.promo}>
              <h1 className="mb-3">Welcome to Clean UI admin template</h1>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad alias aliquid at autem
                commodi corporis, distinctio dolore eveniet explicabo facere iste laborum nobis
                officiis, placeat praesentium quia, sit soluta vel!
              </p>
            </div>
            <div className={styles.form}>
              <p className={styles.formTitle}>Join</p>
              <SignUpForm />
            </div>
          </div>
          <footer className={styles.footer}>
            <ul className={styles.footerNav}>
              <li>
                <a href="javascript: void(0);">Terms of Use</a>
              </li>
              <li>
                <a href="javascript: void(0);">Compliance</a>
              </li>
              <li>
                <a href="javascript: void(0);">Confidential Information</a>
              </li>
              <li>
                <a href="javascript: void(0);">Support</a>
              </li>
              <li>
                <a href="javascript: void(0);">Contacts</a>
              </li>
            </ul>
          </footer>
        </section>
      </div>
    )
  }
}

export default Register
