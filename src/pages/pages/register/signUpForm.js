import React, { Component } from 'react'
import { Button } from 'antd' // Form, Input,
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { compose } from 'redux'
import { push } from 'react-router-redux'

// import {signIn} from './authActions';
// import {bindActionCreators,compose} from 'redux'

class SignUpForm extends Component {
  onSubmit = formProps => {
    const { dispatch } = this.props
    dispatch({
      type: 'user/REGISTER',
      payload: formProps,
    })
  }

  redirectToLogin = () => {
    const { dispatch } = this.props
    dispatch(push('/user/login'))
  }

  signUpWithFacebook = () => {
    const { dispatch } = this.props
    dispatch({ type: 'user/REGISTER_WITH_FACEBOOK' })
  }

  signUpWithTwitter = () => {
    const { dispatch } = this.props
    dispatch({ type: 'user/REGISTER_WITH_TWITTER' })
  }

  signUpWithGoogle = () => {
    const { dispatch } = this.props
    dispatch({ type: 'user/REGISTER_WITH_GOOGLE' })
  }

  render() {
    const {
      form,
      handleSubmit,
      user: { fetching },
    } = this.props

    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit)}>
          <div className="form-group">
            <Field
              id="firstName"
              name="firstName"
              component="input"
              type="text"
              className="form-control"
              aria-describedby="firstName"
              placeholder="FIRST NAME"
              style={{ margin: '0px', fontSize: '12px' }}
            />
          </div>
          <div className="form-group">
            <Field
              id="lastName"
              name="lastName"
              component="input"
              type="text"
              className="form-control"
              aria-describedby="lastName"
              placeholder="LAST NAME"
              style={{ margin: '0px', fontSize: '12px' }}
            />
          </div>
          <div className="form-group">
            <Field
              id="email"
              name="email"
              component="input"
              type="text"
              className="form-control"
              aria-describedby="emailHelp"
              placeholder="EMAIL"
              style={{ margin: '0px', fontSize: '12px' }}
            />
          </div>
          <div className="form-group">
            <Field
              id="password"
              name="password"
              component="input"
              type="password"
              className="form-control"
              placeholder="PASSWORD"
              style={{ margin: '0px', fontSize: '12px' }}
            />
          </div>
          <div className="form-group">
            <Field
              id="passwordConfirm"
              name="passwordConfirm"
              component="input"
              type="password"
              className="form-control"
              placeholder="CONFIRM PASSWORD"
              style={{ margin: '0px', fontSize: '12px' }}
            />
          </div>
          <Button type="primary" className="width-150 mr-4" htmlType="submit" loading={fetching}>
            Register
          </Button>
          <a
            href="javascript: void(0);"
            className="text-primary utils__link"
            onClick={this.redirectToLogin}
          >
            Login
          </a>{' '}
          if you have an account
        </form>
        <div className="form-group">
          <p>Or use another service to sign up</p>
          <div className="mt-2">
            <a
              href="javascript: void(0);"
              className="btn btn-icon mr-2"
              onClick={this.signUpWithFacebook}
            >
              <i className="icmn-facebook" />
            </a>
            <a
              href="javascript: void(0);"
              className="btn btn-icon mr-2"
              onClick={this.signUpWithGoogle}
            >
              <i className="icmn-google" />
            </a>
            <a
              href="javascript: void(0);"
              className="btn btn-icon mr-2"
              onClick={this.signUpWithTwitter}
            >
              <i className="icmn-twitter" />
            </a>
          </div>
        </div>
      </div>
    )
  }
}

const validate = formProps => {
  const errors = {}

  if (!formProps.firstName) {
    errors.firstName = 'Please enter an First Name'
  }

  if (!formProps.lastName) {
    errors.lastName = 'Please enter an Last Name'
  }

  if (!formProps.email) {
    errors.email = 'Please enter an email'
  } else if (!validateEmail(formProps.email)) {
    errors.email = 'Please enter valid email address'
  }

  if (!formProps.password) {
    errors.password = 'Please enter an password'
  } else {
    errors.password = validatePassword(formProps.password)
  }

  if (!formProps.passwordConfirm) {
    errors.passwordConfirm = 'Please confirm password'
  }

  if (formProps.password !== formProps.passwordConfirm) {
    errors.password = 'Passwords must match'
  }

  return errors
}

const validateEmail = email => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

const validatePassword = pw => {
  let error = ''
  const illegalChars = /[\W_]/ // allow only letters and numbers

  if (pw.length < 7 || pw.length > 15) {
    error = 'The password is the wrong length. \n'
  } else if (illegalChars.test(pw)) {
    error = 'The password contains illegal characters.\n'
  } else if (pw.search(/[a-zA-Z]+/) === -1 || pw.search(/[0-9]+/) === -1) {
    error = 'The password must contain at least one numeral.\n'
  }

  return error
}

const mapStateToProps = state => ({ user: state.user })

// const mapDispatchToProps = dispatch => {
//   return {actions: bindActionCreators({signIn: signIn}, dispatch)}
// }

export default compose(
  connect(
    mapStateToProps,
    null,
  ),
  reduxForm({ form: 'signUpForm', validate }),
)(SignUpForm)
