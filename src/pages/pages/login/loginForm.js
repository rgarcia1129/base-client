import React, { Component } from 'react'
import { Button } from 'antd' // Form, Input,
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { compose } from 'redux'
import { push } from 'react-router-redux'

// import {signIn} from './authActions';
// import {bindActionCreators,compose} from 'redux'

class LoginForm extends Component {
  componentWillMount() {
    const { dispatch } = this.props
    dispatch({
      type: 'user/LOGOUT',
    })
  }

  onSubmit = formProps => {
    const { dispatch } = this.props
    dispatch({
      type: 'user/LOGIN',
      payload: formProps,
    })
  }

  redirectToRegister = () => {
    const { dispatch } = this.props
    dispatch(push('/user/register'))
  }

  signUpWithFacebook = () => {
    const { dispatch } = this.props
    dispatch({ type: 'user/REGISTER_WITH_FACEBOOK' })
  }

  signUpWithTwitter = () => {
    const { dispatch } = this.props
    dispatch({ type: 'user/REGISTER_WITH_TWITTER' })
  }

  signUpWithGoogle = () => {
    const { dispatch } = this.props
    dispatch({ type: 'user/REGISTER_WITH_GOOGLE' })
  }

  render() {
    const {
      form,
      handleSubmit,
      user: { fetching },
    } = this.props

    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit)}>
          <div className="form-group">
            <Field
              id="email"
              name="email"
              component="input"
              type="text"
              className="form-control"
              aria-describedby="emailHelp"
              placeholder="EMAIL"
              style={{ margin: '0px', fontSize: '12px' }}
            />
          </div>
          <div className="form-group">
            <Field
              id="password"
              name="password"
              component="input"
              type="password"
              className="form-control"
              placeholder="PASSWORD"
              style={{ margin: '0px', fontSize: '12px' }}
            />
          </div>
          <Button type="primary" className="width-150 mr-4" htmlType="submit" loading={fetching}>
            Log in
          </Button>
          <a
            href="javascript: void(0);"
            className="text-primary utils__link"
            onClick={this.redirectToRegister}
          >
            Register
          </a>{' '}
          if you don&#39;t have account
        </form>
        <div className="form-group">
          <p>Or use another service to sign up</p>
          <div className="mt-2">
            <a
              href="javascript: void(0);"
              className="btn btn-icon mr-2"
              onClick={this.signUpWithFacebook}
            >
              <i className="icmn-facebook" />
            </a>
            <a
              href="javascript: void(0);"
              className="btn btn-icon mr-2"
              onClick={this.signUpWithGoogle}
            >
              <i className="icmn-google" />
            </a>
            <a
              href="javascript: void(0);"
              className="btn btn-icon mr-2"
              onClick={this.signUpWithTwitter}
            >
              <i className="icmn-twitter" />
            </a>
          </div>
        </div>
      </div>
    )
  }
}

const validate = formProps => {
  const errors = {}

  if (!formProps.email) {
    errors.email = 'Please enter an email'
  }

  if (!formProps.password) {
    errors.password = 'Please enter an password'
  }

  return errors
}

const mapStateToProps = state => ({ user: state.user })

// const mapDispatchToProps = dispatch => {
//   return {actions: bindActionCreators({signIn: signIn}, dispatch)}
// }

export default compose(
  connect(
    mapStateToProps,
    null,
  ),
  reduxForm({ form: 'loginForm', validate }),
)(LoginForm)
