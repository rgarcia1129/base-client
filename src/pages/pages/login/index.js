import React from 'react'
import { Helmet } from 'react-helmet'
import styles from './style.module.scss'
import LoginForm from './loginForm'

class LoginAlpha extends React.Component {
  render() {
    return (
      <div>
        <Helmet title="Login" />
        <section
          className={`${styles.login} ${styles.fullscreen}`}
          style={{ backgroundImage: `url('resources/images/photos/1.jpeg')` }}
        >
          <header className={styles.header}>
            <a className={styles.logo} href="javascript: void(0);">
              <img src="resources/images/logo-inverse.png" alt="Clean UI Admin Template" />
            </a>
            <nav className={styles.loginNav}>
              <ul className={styles.navItems}>
                <li>
                  <a href="javascript: void(0);">&larr; Back</a>
                </li>
                <li>
                  <a className={styles.active} href="javascript: void(0);">
                    Login
                  </a>
                </li>
                <li>
                  <a href="javascript: void(0);">About</a>
                </li>
                <li>
                  <a href="javascript: void(0);">Support</a>
                </li>
              </ul>
            </nav>
          </header>
          <div className={styles.content}>
            <div className={styles.promo}>
              <h1 className="mb-3">Welcome to Clean UI admin template</h1>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                Ipsum has been the industry&#39;s standard dummy text ever since the 1500s.
              </p>
            </div>
            <div className={styles.form}>
              <p className={styles.formTitle}>Welcome Back</p>
              <LoginForm />
            </div>
          </div>
          <footer className={styles.footer}>
            <ul className={styles.footerNav}>
              <li>
                <a href="javascript: void(0);">Terms of Use</a>
              </li>
              <li>
                <a href="javascript: void(0);">Compliance</a>
              </li>
              <li>
                <a href="javascript: void(0);">Confidential Information</a>
              </li>
              <li>
                <a href="javascript: void(0);">Support</a>
              </li>
              <li>
                <a href="javascript: void(0);">Contacts</a>
              </li>
            </ul>
          </footer>
        </section>
      </div>
    )
  }
}

export default LoginAlpha
