import React from 'react'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import Avatar from 'components/CleanUIComponents/Avatar'
import styles from './style.module.scss'

const mapStateToProps = state => ({ user: state.user })

@connect(
  mapStateToProps,
  null,
)
class Lockscreen extends React.Component {
  resendVerificationEmail = () => {
    console.log('resend email')
    const { dispatch } = this.props
    dispatch({
      type: 'user/SEND_EMAIL_VERIFICATION',
    })
  }

  render() {
    const { user } = this.props
    return (
      <div>
        <Helmet title="Verify Email" />
        <section
          className={`${styles.login} ${styles.fullscreen}`}
          style={{ backgroundImage: `url('resources/images/photos/1.jpeg')` }}
        >
          <header className={styles.header}>
            <a className={styles.logo} href="javascript: void(0);">
              <img src="resources/images/logo-inverse.png" alt="Clean UI Admin Template" />
            </a>
          </header>
          <div className={styles.content}>
            <div className={styles.form}>
              <div className={styles.user}>
                <Avatar src="resources/images/avatars/1.jpg" border="true" size="90" />
                <p>
                  <i className="icmn-lock" />
                  <strong>{user.displayName}</strong>
                </p>
              </div>
              <p>
                An email confirmation has been sent to {user.email}. Please click the link to verify
                before entering the site.
              </p>
              <div className="form-actions text-center">
                <button
                  type="submit"
                  className="btn btn-success"
                  onClick={this.resendVerificationEmail}
                >
                  Resend email
                </button>
              </div>
              {/* <div className="text-center">
                <a href="javascript: void(0);" className="text-default">
                  Or sign in as a different user
                </a>
              </div> */}
            </div>
          </div>
        </section>
      </div>
    )
  }
}

export default Lockscreen
